<?php

declare(strict_types=1);

namespace App\Shared\Container;

use Psr\Container\ContainerInterface;

final class Container implements ContainerInterface
{
    /**
     * @var array<object>
     */
    private array $instances = [];

    public function add(string $className, object $instance): void
    {
        $this->instances[$className] = $instance;
    }

    public function get($id)
    {
        if (!isset($this->instances[$id])) {
            throw new Exception\NotFoundException($id);
        }

        return $this->instances[$id];
    }

    public function has($id): bool
    {
        return isset($this->instances[$id]);
    }
}
