<?php

declare(strict_types=1);

namespace App\Shared\Container\Exception;

use Psr\Container\NotFoundExceptionInterface;

final class NotFoundException extends \RuntimeException implements NotFoundExceptionInterface
{
}
