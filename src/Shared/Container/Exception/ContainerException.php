<?php

declare(strict_types=1);

namespace App\Shared\Container\Exception;

use Psr\Container\ContainerExceptionInterface;

final class ContainerException extends \RuntimeException implements ContainerExceptionInterface
{
}
