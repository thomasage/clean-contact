<?php

namespace App\Contact\Application\ListContact;

interface ListContact
{
    public function handle(ListContactRequest $request, ListContactPresenter $presenter): void;
}
