<?php

namespace App\Contact\Application\ListContact;

final class ContactOutput
{
    public string $fullname;
    public ?string $birthday;
}
