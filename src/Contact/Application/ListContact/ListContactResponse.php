<?php

namespace App\Contact\Application\ListContact;

final class ListContactResponse
{
    /**
     * @var array<ContactOutput>
     */
    public array $contacts = [];
}
