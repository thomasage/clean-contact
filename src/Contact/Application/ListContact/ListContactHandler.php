<?php

declare(strict_types=1);

namespace App\Contact\Application\ListContact;

use App\Contact\Domain\Entity\Contact;
use App\Contact\Domain\Gateway\ContactProvider;

final class ListContactHandler implements ListContact
{
    private ContactProvider $provider;

    public function __construct(ContactProvider $provider)
    {
        $this->provider = $provider;
    }

    private static function toDto(Contact $contact): ContactOutput
    {
        $output = new ContactOutput();
        $output->birthday = self::getBirthday($contact->getBirthday());
        $output->fullname = $contact->getFullname();

        return $output;
    }

    private static function getBirthday(?\DateTimeImmutable $birthday): ?string
    {
        return $birthday ? $birthday->format('Y-m-d') : null;
    }

    public function handle(ListContactRequest $request, ListContactPresenter $presenter): void
    {
        $response = new ListContactResponse();
        $response->contacts = array_map([__CLASS__, 'toDto'], iterator_to_array($this->provider->getAll()));

        $presenter->present($response);
    }
}
