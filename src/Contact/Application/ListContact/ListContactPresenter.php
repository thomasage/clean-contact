<?php

namespace App\Contact\Application\ListContact;

interface ListContactPresenter
{
    public function present(ListContactResponse $response): void;
}
