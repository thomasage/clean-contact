<?php

namespace App\Contact\Application\AddContact;

final class AddContactRequest
{
    public string $firstname = '';
    public string $lastname = '';
    public ?\DateTimeImmutable $birthday = null;
}
