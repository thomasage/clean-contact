<?php

namespace App\Contact\Application\AddContact;

interface AddContact
{
    public function handle(AddContactRequest $request, AddContactPresenter $presenter): void;
}
