<?php

declare(strict_types=1);

namespace App\Contact\Application\AddContact;

use App\Contact\Domain\Entity\Contact;
use App\Contact\Domain\Gateway\ContactIdGenerator;
use App\Contact\Domain\Gateway\ContactPersister;

final class AddContactHandler implements AddContact
{
    private ContactPersister $persister;
    private ContactIdGenerator $idGenerator;

    public function __construct(ContactPersister $persister, ContactIdGenerator $idGenerator)
    {
        $this->persister = $persister;
        $this->idGenerator = $idGenerator;
    }

    public function handle(AddContactRequest $request, AddContactPresenter $presenter): void
    {
        $response = new AddContactResponse();
        $this->addContact($request, $response);
        $presenter->present($response);
    }

    private function addContact(AddContactRequest $request, AddContactResponse $response): void
    {
        if ('' === $request->firstname || '' === $request->lastname) {
            return;
        }

        $contact = new Contact($this->idGenerator->generate(), $request->firstname, $request->lastname);
        $contact->setBirthday($request->birthday);

        $this->persister->save($contact);

        $response->added = true;
    }
}
