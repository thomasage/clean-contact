<?php

namespace App\Contact\Application\AddContact;

interface AddContactPresenter
{
    public function present(AddContactResponse $response): void;
}
