<?php

namespace App\Contact\Application\AddContact;

final class AddContactResponse
{
    public bool $added = false;
}
