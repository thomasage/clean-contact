<?php

declare(strict_types=1);

namespace App\Contact\Domain\Entity;

final class Contact
{
    private string $id;
    private string $firstname;
    private string $lastname;
    private ?\DateTimeImmutable $birthday = null;

    public function __construct(string $id, string $firstname, string $lastname)
    {
        $this->id = $id;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
    }

    public function getFullname(): string
    {
        return sprintf('%s %s', $this->firstname, $this->lastname);
    }

    public function getBirthday(): ?\DateTimeImmutable
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeImmutable $birthday): void
    {
        $this->birthday = $birthday;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function getId(): string
    {
        return $this->id;
    }
}
