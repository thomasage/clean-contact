<?php

namespace App\Contact\Domain\Gateway;

use App\Contact\Domain\Entity\Contact;

interface ContactProvider
{
    /**
     * @return \Generator<Contact>
     */
    public function getAll(): \Generator;
}
