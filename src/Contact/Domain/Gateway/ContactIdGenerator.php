<?php

namespace App\Contact\Domain\Gateway;

interface ContactIdGenerator
{
    public function generate(): string;
}
