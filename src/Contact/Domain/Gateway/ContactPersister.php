<?php

namespace App\Contact\Domain\Gateway;

use App\Contact\Domain\Entity\Contact;

interface ContactPersister
{
    public function save(Contact $contact): void;
}
