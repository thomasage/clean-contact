<?php

declare(strict_types=1);

namespace App\Contact\UI\View;

use App\Contact\UI\ViewModel\ListContactHtmlViewModel;

final class ListContactHtmlView
{
    public function render(ListContactHtmlViewModel $vm): void
    {
        require_once __DIR__.'/../../../../templates/contact_list.html.php';
    }
}
