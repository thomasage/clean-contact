<?php

declare(strict_types=1);

namespace App\Contact\UI\View;

use App\Contact\UI\ViewModel\AddContactHtmlViewModel;

final class AddContactHtmlView
{
    public function render(AddContactHtmlViewModel $vm): void
    {
        if ($vm->added) {
            header('location:/');
            exit();
        }

        require_once __DIR__.'/../../../../templates/contact_add.html.php';
    }
}
