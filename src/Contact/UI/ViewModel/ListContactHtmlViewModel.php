<?php

namespace App\Contact\UI\ViewModel;

final class ListContactHtmlViewModel
{
    /**
     * @var array<ListContactHtmlContact>
     */
    public array $contacts = [];
    public string $pageTitle = 'List contacts';
}
