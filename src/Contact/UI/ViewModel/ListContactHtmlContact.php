<?php

namespace App\Contact\UI\ViewModel;

final class ListContactHtmlContact
{
    public string $birthday;
    public string $fullname;
}
