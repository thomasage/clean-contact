<?php

namespace App\Contact\UI\ViewModel;

final class AddContactHtmlViewModel
{
    public bool $added = false;
    public string $birthday = '';
    public string $firstname = '';
    public string $lastname = '';
    public string $pageTitle = 'Add contact';
}
