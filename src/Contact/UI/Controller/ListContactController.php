<?php

declare(strict_types=1);

namespace App\Contact\UI\Controller;

use App\Contact\Application\ListContact\ListContact;
use App\Contact\Application\ListContact\ListContactRequest;
use App\Contact\UI\Presenter\ListContactHtmlPresenter;
use Psr\Container\ContainerInterface;

final class ListContactController
{
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(): void
    {
        $request = new ListContactRequest();

        $presenter = new ListContactHtmlPresenter();

        $handler = $this->container->get(ListContact::class);
        $handler->handle($request, $presenter);
    }
}
