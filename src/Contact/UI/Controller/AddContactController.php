<?php

declare(strict_types=1);

namespace App\Contact\UI\Controller;

use App\Contact\Application\AddContact\AddContact;
use App\Contact\Application\AddContact\AddContactRequest;
use App\Contact\UI\Presenter\AddContactHtmlPresenter;
use Psr\Container\ContainerInterface;

final class AddContactController
{
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(): void
    {
        $request = new AddContactRequest();
        self::handlePostData($request);

        $presenter = new AddContactHtmlPresenter();

        $handler = $this->container->get(AddContact::class);
        $handler->handle($request, $presenter);
    }

    private static function handlePostData(AddContactRequest $request): void
    {
        if (null === filter_input(INPUT_POST, 'add')) {
            return;
        }
        $request->firstname = filter_input(INPUT_POST, 'firstname');
        $request->lastname = filter_input(INPUT_POST, 'lastname');
        $birthday = filter_input(INPUT_POST, 'birthday');
        if ($birthday) {
            $birthday = \DateTimeImmutable::createFromFormat('Y-m-d', $birthday);
        }
        if ($birthday) {
            $request->birthday = $birthday;
        }
    }
}
