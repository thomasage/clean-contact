<?php

declare(strict_types=1);

namespace App\Contact\UI\Presenter;

use App\Contact\Application\ListContact\ContactOutput;
use App\Contact\Application\ListContact\ListContactPresenter;
use App\Contact\Application\ListContact\ListContactResponse;
use App\Contact\UI\View\ListContactHtmlView;
use App\Contact\UI\ViewModel\ListContactHtmlContact;
use App\Contact\UI\ViewModel\ListContactHtmlViewModel;

final class ListContactHtmlPresenter implements ListContactPresenter
{
    private static function toDto(ContactOutput $contact): ListContactHtmlContact
    {
        $dto = new ListContactHtmlContact();
        $dto->birthday = $contact->birthday ? date('m/d/Y', (int) strtotime($contact->birthday)) : '';
        $dto->fullname = $contact->fullname;

        return $dto;
    }

    public function present(ListContactResponse $response): void
    {
        $viewModel = new ListContactHtmlViewModel();
        $viewModel->contacts = array_map([__CLASS__, 'toDto'], $response->contacts);
        $view = new ListContactHtmlView();
        $view->render($viewModel);
    }
}
