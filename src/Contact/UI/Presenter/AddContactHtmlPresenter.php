<?php

declare(strict_types=1);

namespace App\Contact\UI\Presenter;

use App\Contact\Application\AddContact\AddContactPresenter;
use App\Contact\Application\AddContact\AddContactResponse;
use App\Contact\UI\View\AddContactHtmlView;
use App\Contact\UI\ViewModel\AddContactHtmlViewModel;

final class AddContactHtmlPresenter implements AddContactPresenter
{
    public function present(AddContactResponse $response): void
    {
        $viewModel = new AddContactHtmlViewModel();
        $viewModel->added = $response->added;
        $view = new AddContactHtmlView();
        $view->render($viewModel);
    }
}
