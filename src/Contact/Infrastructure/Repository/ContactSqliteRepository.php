<?php

declare(strict_types=1);

namespace App\Contact\Infrastructure\Repository;

use App\Contact\Domain\Entity\Contact;
use App\Contact\Domain\Gateway\ContactPersister;
use App\Contact\Domain\Gateway\ContactProvider;

final class ContactSqliteRepository implements ContactProvider, ContactPersister
{
    private \PDO $db;

    public function __construct(string $filename)
    {
        $this->db = new \PDO(sprintf('sqlite:%s', $filename));
        $this->db->setAttribute(\PDO::ATTR_CASE, \PDO::CASE_NATURAL);
        $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function getAll(): \Generator
    {
        $result = $this->db->query('SELECT * FROM contacts ORDER BY firstname, lastname');
        if (!$result) {
            return;
        }
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            yield self::toEntity($row);
        }
        $result->closeCursor();
    }

    /**
     * @param array<mixed> $result
     */
    private static function toEntity(array $result): Contact
    {
        $contact = new Contact($result['id'], $result['firstname'], $result['lastname']);
        $contact->setBirthday(self::getBirthday((string) $result['birthday']));

        return $contact;
    }

    private static function getBirthday(string $birthday): ?\DateTimeImmutable
    {
        if ('' === $birthday) {
            return null;
        }
        $birthdayDT = \DateTimeImmutable::createFromFormat('Y-m-d', $birthday);

        return false === $birthdayDT ? null : $birthdayDT;
    }

    public function save(Contact $contact): void
    {
        $birthday = $contact->getBirthday();
        if ($birthday) {
            $birthday = $birthday->format('Y-m-d');
        }
        $result = $this->db->prepare(
            'INSERT INTO contacts ( id, firstname, lastname, birthday )
             VALUES ( ?, ?, ?, ? )'
        );
        $result->execute([$contact->getId(), $contact->getFirstname(), $contact->getLastname(), $birthday]);
        $result->closeCursor();
    }
}
