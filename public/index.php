<?php

declare(strict_types=1);

use App\Contact\Application\AddContact\AddContact;
use App\Contact\Application\AddContact\AddContactHandler;
use App\Contact\Application\ListContact\ListContact;
use App\Contact\Application\ListContact\ListContactHandler;
use App\Contact\Domain\Gateway\ContactIdGenerator;
use App\Contact\Domain\Gateway\ContactPersister;
use App\Contact\Domain\Gateway\ContactProvider;
use App\Contact\Infrastructure\Generator\ContactIdNativeGenerator;
use App\Contact\Infrastructure\Repository\ContactSqliteRepository;
use App\Contact\UI\Controller\AddContactController;
use App\Contact\UI\Controller\ListContactController;
use App\Shared\Container\Container;

ini_set('display_errors', 'On');
error_reporting(-1);

require_once __DIR__.'/../vendor/autoload.php';

// TODO: should be in a config file
$database = __DIR__.'/../var/contacts.sqlite';

$container = new Container();
$container->add(ContactIdGenerator::class, new ContactIdNativeGenerator());
$container->add(ContactPersister::class, $contactRepository = new ContactSqliteRepository($database));
$container->add(ContactProvider::class, $contactRepository);
$container->add(
    AddContact::class,
    new AddContactHandler($container->get(ContactPersister::class), $container->get(ContactIdGenerator::class))
);
$container->add(ListContact::class, new ListContactHandler($container->get(ContactProvider::class)));

$url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

if ('/add' === $url) {
    $controller = new AddContactController($container);
} else {
    $controller = new ListContactController($container);
}
$controller();
