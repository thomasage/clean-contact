EXEC_COMPOSER = composer
EXEC_SYMFONY = symfony

.PHONY = cs deptrac help install phpstan

.DEFAULT_GOAL := help

##

help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

install: composer.lock ## Install vendors according to the current composer.lock file
	@$(EXEC_COMPOSER) install

##

cs: ## Fix code style
	@$(EXEC_SYMFONY) run vendor/bin/php-cs-fixer fix

deptrac: ## Analyse dependencies
	@$(EXEC_SYMFONY) run vendor/bin/deptrac \
		--formatter-graphviz=false \
		--report-uncovered \
		--fail-on-uncovered

phpstan: ## Static analysis
	@$(EXEC_SYMFONY) run vendor/bin/phpstan analyse

test: ## Unit and functional tests
	@$(EXEC_SYMFONY) run vendor/bin/phpunit

##
