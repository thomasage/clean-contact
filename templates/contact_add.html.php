<?php
declare(strict_types=1);

/**
 * @var AddContactHtmlViewModel $vm
 */

use App\Contact\UI\ViewModel\AddContactHtmlViewModel;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($vm->pageTitle); ?></title>
    <link type="text/css" rel="stylesheet" href="/app.css"/>
</head>
<body>
<h1><?php echo htmlentities($vm->pageTitle); ?></h1>
<nav>
    <a href="/">Return to the list</a>
</nav>
<form method="post">
    <div>
        <label for="firstname">Firstname</label>
        <input type="text"
               name="firstname"
               value="<?php echo htmlentities($vm->firstname); ?>"
               id="firstname"
               required
               autofocus/>
    </div>
    <div>
        <label for="lastname">Lastname</label>
        <input type="text"
               name="lastname"
               value="<?php echo htmlentities($vm->lastname); ?>"
               id="lastname"
               required/>
    </div>
    <div>
        <label for="birthday">Birthday</label>
        <input type="date"
               name="birthday"
               value="<?php echo htmlentities($vm->birthday); ?>"
               id="birthday"/>
    </div>
    <div>
        <button type="submit" name="add">Add contact</button>
    </div>
</form>
</body>
</html>
