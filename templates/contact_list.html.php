<?php
declare(strict_types=1);

/**
 * @var ListContactHtmlViewModel $vm
 */

use App\Contact\UI\ViewModel\ListContactHtmlViewModel;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($vm->pageTitle); ?></title>
    <link type="text/css" rel="stylesheet" href="/app.css"/>
</head>
<body>
<h1><?php echo htmlentities($vm->pageTitle); ?></h1>
<nav>
    <a href="/add">Add a contact</a>
</nav>
<table>
    <thead>
    <tr>
        <th>Fullname</th>
        <th>Birthday</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($vm->contacts as $contact): ?>
        <tr>
            <td><?php echo htmlentities($contact->fullname); ?></td>
            <td><?php echo htmlentities($contact->birthday); ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</body>
</html>
